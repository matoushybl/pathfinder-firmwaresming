#include <user_config.h>
#include <SmingCore/SmingCore.h>
#include "HardwareUtils.h"

HardwareUtils hardwareUtils;

MqttClient *mqttClient;

Timer mqttTimer;

void mqttCallback(String topic, String message);
void mqttCheckCallback();

void smartConfigCallback(sc_status status, void *pdata);

void wifiConnected();
void wifiFailed();

void onManualTrigger();
void onReset();

void init() {
    pinMode(POWER_PIN, OUTPUT);
    pinMode(TRIG_PIN, INPUT);
    pinMode(RESET_PIN, INPUT);
    pinMode(POSITIVE_PIN, OUTPUT);
    pinMode(NEGATIVE_PIN, OUTPUT);

    attachInterrupt(TRIG_PIN, onManualTrigger, FALLING);
    attachInterrupt(RESET_PIN, onReset, FALLING);

    // light up the negative pin
    hardwareUtils.indicatePositiveStatus(false);

    spiffs_mount();

    Serial.begin(9600);
    Serial.println("Pathfinder Plug.");

    WifiAccessPoint.enable(false);

    if(WifiStation.getSSID().length() == 0) {
        Serial.println("WIFI not configured, starting SmartConfig.");
        WifiStation.enable(true);
        WifiStation.smartConfigStart(SCT_EspTouch, smartConfigCallback);
    } else {
        Serial.printf("Configuring WIFI using: %s; %s\n", WifiStation.getSSID().c_str(), WifiStation.getPassword().c_str());
        WifiStation.enable(true);
        WifiStation.waitConnection(wifiConnected, 30, wifiFailed);
    }
}

void initMqtt() {
    mqttClient = new MqttClient("85.207.23.18", 1883, mqttCallback);
    mqttClient->setKeepAlive(3600);
    mqttTimer.initializeMs(1000, mqttCheckCallback).start();
}

void mqttCallback(String topic, String message) {
    Serial.printf("MQTT MSG: %s:%s\n", topic.c_str(), message.c_str());
    if(topic == "power/toggle") {
        hardwareUtils.togglePower();
    } else if(topic == "power/toggle/on") {
        hardwareUtils.powerOn();
    } else if(topic == "power/toggle/off") {
        hardwareUtils.powerOff();
    }
}

void mqttCheckCallback() {
    if(WifiStation.isConnected() && mqttClient->getConnectionState() != eTCS_Connected && mqttClient->getConnectionState() != eTCS_Connecting) {
        mqttClient->connect("SmingClient");
        mqttClient->subscribe("power/toggle");
        mqttClient->subscribe("power/toggle/on");
        mqttClient->subscribe("power/toggle/off");
        hardwareUtils.indicatePositiveStatus(true);
    } else if(mqttClient->getConnectionState() == mqttClient->getConnectionState() == eTCS_Failed){
        hardwareUtils.indicatePositiveStatus(false);
    }
}

void smartConfigCallback(sc_status status, void *pdata) {
    switch (status) {
        case SC_STATUS_WAIT:
        case SC_STATUS_FIND_CHANNEL:
        case SC_STATUS_GETTING_SSID_PSWD:
            break;
        case SC_STATUS_LINK: {
            debugf("SC_STATUS_LINK\n");
            station_config *sta_conf = (station_config *) pdata;
            char *ssid = (char *) sta_conf->ssid;
            char *password = (char *) sta_conf->password;
            WifiStation.config(ssid, password, true);
        }
            break;
        case SC_STATUS_LINK_OVER:
            debugf("SC_STATUS_LINK_OVER\n");
            WifiStation.smartConfigStop();
            initMqtt();
            break;
    }
}

void wifiConnected() {
    Serial.println("Successfully connected to WiFi.");
    initMqtt();
}

void wifiFailed() {
    Serial.println("WiFi connection failed.");
    Serial.println(WifiStation.getIP().toString());
    WifiStation.waitConnection(wifiConnected, 30, wifiFailed);
}

void onManualTrigger() {
    // FIXME publish retained message
    hardwareUtils.togglePower();
}

void onReset() {
    Serial.println("Resetting WIFI settings. Rebooting.");
    WifiStation.config("", "", true);
    System.restart();
}