//
// Created by Matouš Hýbl on 10/01/16.
//

#include "HardwareUtils.h"
#include <SmingCore/SmingCore.h>

void HardwareUtils::indicatePositiveStatus(bool positive) {
    digitalWrite(POSITIVE_PIN, (uint8_t) (positive ? HIGH : LOW));
    digitalWrite(NEGATIVE_PIN, (uint8_t) (positive ? LOW : HIGH));
}

void HardwareUtils::togglePower() {
    digitalWrite(POWER_PIN, (uint8_t) (digitalRead(POWER_PIN) == HIGH ? LOW : HIGH));
}

void HardwareUtils::powerOn() {
    digitalWrite(POWER_PIN, HIGH);
}

void HardwareUtils::powerOff() {
    digitalWrite(POWER_PIN, LOW);
}
