//
// Created by Matouš Hýbl on 10/01/16.
//

#ifndef PATHFINDER_HARDWAREUTILS_H
#define PATHFINDER_HARDWAREUTILS_H

// FIXME use ifndefs to change board
#define POWER_PIN 5
#define TRIG_PIN 13
#define RESET_PIN 4
#define POSITIVE_PIN 16
#define NEGATIVE_PIN 14

#ifdef TEMP
    #define TEMP_PIN 5
#endif

class HardwareUtils {

public:
    void indicatePositiveStatus(bool positive);

    void togglePower();

    void powerOn();

    void powerOff();
};


#endif //PATHFINDER_HARDWAREUTILS_H
